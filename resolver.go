package main

import (
	"database/sql"
	"fmt"
	"log"
)

type Resolver struct{}

//Punchline

type Punchline struct {
	ID       int32   `json:"id"`
	Content  *string `json:"content"`
	AuthorID int32   `json:"author_id"`
}

type PunchlineResolver struct {
	p Punchline
}

func (r *PunchlineResolver) ID() int32 {
	return r.p.ID
}

func (r *PunchlineResolver) Content() *string {
	return r.p.Content
}

func (r *PunchlineResolver) AuthorID() int32 {
	return r.p.AuthorID
}

//Author

type Author struct {
	ID        int32
	LastName  *string
	FirstName *string
	Surname   *string
}

type AuthorResolver struct {
	a Author
}

func (r *AuthorResolver) ID() int32 {
	return r.a.ID
}

func (r *AuthorResolver) LastName() *string {
	return r.a.LastName
}

func (r *AuthorResolver) FirstName() *string {
	return r.a.FirstName
}

func (r *AuthorResolver) Surname() *string {
	return r.a.Surname
}

//Quiz

type Quiz struct {
	Authors   []*AuthorResolver
	Punchline *PunchlineResolver
}

type QuizResolver struct {
	qr Quiz
}

func (r *QuizResolver) Authors() []*AuthorResolver {
	return r.qr.Authors
}

func (r *QuizResolver) Punchline() *PunchlineResolver {
	return r.qr.Punchline
}

func (r *Resolver) GetQuizz() *QuizResolver {
	db := NewDatabase()
	defer db.Close()
	var quiz Quiz

	//Get authors
	sqlStatement := `SELECT * FROM authors  ORDER BY random() LIMIT 4 `
	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var authors []*AuthorResolver

	for rows.Next() {
		author := Author{}
		err = rows.Scan(&author.ID, &author.LastName, &author.FirstName, &author.Surname)
		if err != nil {
			panic(err)
		}
		authors = append(authors, &AuthorResolver{author})
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	quiz.Authors = authors
	fmt.Println(authors)

	//Get Punchline
	punchline := Punchline{}

	sqlStatement = `SELECT id, content, author_id FROM punchlines WHERE author_id = $1 ORDER BY random() LIMIT 1`
	row := db.QueryRow(sqlStatement, quiz.Authors[0].a.ID)
	err = row.Scan(&punchline.ID, &punchline.Content, &punchline.AuthorID)

	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Zero rows found")
		} else {
			panic(err)
		}
	}

	// BUILD QUIZ

	quiz.Punchline = &PunchlineResolver{punchline}

	resolver := &QuizResolver{quiz}

	return resolver
}
