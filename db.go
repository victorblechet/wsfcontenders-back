package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/subosito/gotenv"

	_ "github.com/lib/pq"
)

func NewDatabase() *sql.DB {

	gotenv.Load()

	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	//String connection DB
	connectionString :=
		fmt.Sprintf("host=%s port=%s "+"user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	fmt.Println(connectionString)
	var err error

	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
