package main

import (
	"bufio"
	"os"
	"path/filepath"
	"strings"
)

func MergeGQL() string {
	var s strings.Builder
	real, _ := filepath.Abs("./schema")
	files, err := filepath.Glob(real + "/*.graphql")
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		file, err := os.Open(f)
		if err != nil {
			panic(err)
		}
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			_, err = s.Write(scanner.Bytes())
			if err != nil {
				panic(err)
			}
			_, err = s.WriteString("\n")
			if err != nil {
				panic(err)
			}
		}
	}
	return s.String()
}
